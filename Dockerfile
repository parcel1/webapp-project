FROM node:14.15.0-alpine3.10
LABEL maintainer="Tran Tuan Anh <anhtt@g-tech.ai>"

# Workdir
WORKDIR /app

RUN apk update \
	&& apk add --no-cache gcc build-base autoconf automake

# Timezone
ENV TZ=Asia/Ho_Chi_Minh
RUN apk add --no-cache tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD . .
# RUN mv .env.example .env

# Install dependences
RUN yarn install
RUN yarn build

# Start
CMD ["/usr/local/bin/yarn", "start"]

EXPOSE 3000
