export default async function ({ store }) {
  if (store.state.category.categories.length === 0) {
    await store.dispatch('category/setCategories')
  }
}
