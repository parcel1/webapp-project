export default async function ({ store }) {
  if (store.state.weather.weather === null) {
    await store.dispatch('weather/setWeather')
  }
}
