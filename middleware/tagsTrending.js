export default async function ({ store }) {
  if (store.state.tagTrending.tags.length === 0) {
    await store.dispatch('tagTrending/setTags')
  }
}
