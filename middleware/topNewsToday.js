export default async function ({ store }) {
  if (store.state.newToday.newsToday.length === 0 || store.state.newToday.videoNewsToday.length === 0) {
    await store.dispatch('newToday/setNewsToday')
  }
}
