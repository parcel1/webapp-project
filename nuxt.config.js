import path from "path";
export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  publicRuntimeConfig: {
    BASE_URL_APP_API: process.env.BASE_URL_APP_API,
  },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  head: {
    title: 'Hit.vn',
    titleTemplate: '%s | Hit: Tin nóng từng giây',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Hit.vn - Cập nhật những thông tin mới nhất trong và ngoài nước : đời sống xã hội, thời sự, showbiz, du lịch, kinh doanh...' },
      { property: "og:image", content: "https://cdn.hit.vn/Hitvn-Logo.png" },
      { name: "apple-itunes-app", content: "app-id=1468232890" },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/style.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: '~/plugins/fragment' }, {
    src: '@/plugins/vue-lazy-load',
    ssr: false,
  },
  { src: '@/plugins/vue-observe-visibility.client.js' }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: [
    '~/components',
  ],

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/dotenv'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/robots',
    'vue-social-sharing/nuxt',
    'nuxt-clipboard2',
    '@nuxt/components',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},
  router: {
    middleware: ['initCategory', 'topNewsToday', 'tagsTrending', 'initWeather'],
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    // analyze: true,
    postcss: {
      plugins: {
        'postcss-import': {},
        tailwindcss: path.resolve(__dirname, './tailwind.config.js'),
        'postcss-nested': {}
      }
    },
    preset: {
      stage: 1 // see https://tailwindcss.com/docs/using-with-preprocessors#future-css-featuress
    },
    extractCSS: {
      ignoreOrder: true
    },
    optimizeCSS: true,
    optimization: {
      splitChunks: {
        chunks: 'async',
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          },
          tailwindConfig: {
            test: /tailwind\.config/,
            chunks: 'all',
            priority: 10,
            name: true
          }
        }
      }
    },
    splitChunks: {
      pages: true,
      vendor: true,
      commons: true,
      runtime: true,
      layouts: true
    },
    html: {
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true
      }
    },
    babel: {
      presets ({ envName }) {
        const envTargets = {
          client: { browsers: ["last 2 versions"], ie: 11 },
          server: { node: "current" },
        }
        return [
          [
            "@nuxt/babel-preset-app",
            {
              targets: envTargets[envName]
            }
          ]
        ]
      }
    },
    extend: (config) => {
      const svgRule = config.module.rules.find((rule) => rule.test.test('.svg'))

      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        use: ['babel-loader', 'vue-svg-loader']
      })
    }
  }
}
