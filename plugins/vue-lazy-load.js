import Vue from 'vue'
import VueLazyLoad from 'vue-lazyload'

Vue.use(VueLazyLoad, {
  lazyComponent: true,
  preLoad: 1.3,
  attempt: 1,
  listenEvents: ['scroll', 'wheel', 'mousewheel'],
  observer: true,
  observerOptions: {
    rootMargin: '0px',
    threshold: 0.1
  }
})
