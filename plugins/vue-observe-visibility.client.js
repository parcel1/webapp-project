import VueObserveVisibility from 'vue-observe-visibility'
import Vue from 'vue'

Vue.use(VueObserveVisibility)
