import axios from 'axios'
// import { getToken } from '@/utils/cookies'

export const service = axios.create({
  baseURL: process.env.BASE_URL_APP_API,
  timeout: 5000
})

// Request interceptors
service.interceptors.request.use(
  (config) => {
    // Add X-Access-Token header to every request, you can add other custom headers here
    // if (getToken) {
    //   config.headers.Authorization = `Bearer ${getToken()}`
    // }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

// Response interceptors
service.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)
