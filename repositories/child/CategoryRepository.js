import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getAllCategories = (profileId) =>
  service({
    url: `/v3.0/profile/${profileId}/news/categories/all`,
    method: 'get'
  })

export const getPostsByCategoryFirst = (pageSize, pageIndex, code) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/categories/${code}/posts/web`,
    method: 'get',
    params: {
      pageIndex,
      pageSize
    }
  })

export const getPostsByCategory = (pageSize, pageIndex, code, lastId) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/categories/${code}/posts/web`,
    method: 'get',
    params: {
      pageIndex,
      pageSize,
      lastId
    }
  })
