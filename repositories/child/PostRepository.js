import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getVideos = (pageSize, pageIndex, lastId) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/videos`,
    method: 'get',
    params: {
      pageIndex,
      pageSize,
      lastId
    }
  })

export const getVideosFirst = (pageSize, pageIndex) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/videos`,
    method: 'get',
    params: {
      pageIndex,
      pageSize
    }
  })

export const getHotPosts = (pageIndex) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/posts/hot/web`,
    method: 'get',
    params: {
      pageIndex
    }
  })

export const getNewsToday = () =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/posts/top/view`,
    method: 'get'
  })

export const getVideoNewsToday = () =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/videos/top/view`,
    method: 'get'
  })

export const getPostDetail = (slug) =>
  service({
    url: `/v4.0/profile/${getCookie('profileId')}/posts/short-id/${slug}`,
    method: 'get'
  })

export const getNewsRecommend = (slug) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/posts/short-id/${slug}/web/recommend`,
    method: 'get'
  })

export const getNewsSimilar = (slug) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/posts/short-id/${slug}/web/similar`,
    method: 'get'
  })

// old
export const getPostById = (id) =>
  service({
    url: `/v4.0/profile/${getCookie('profileId')}/posts/${id}/web`,
    method: 'get'
  })
