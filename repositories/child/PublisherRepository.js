import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getPublisherDetail = (id) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/publishers/${id}`,
    method: 'get'
  })

export const getPostsByPublisherFirst = (pageSize, pageIndex, code) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/publishers/code/${code}/posts`,
    method: 'get',
    params: {
      pageIndex,
      pageSize
    }
  })

export const getPostsByPublisher = (pageSize, pageIndex, code, lastId) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/publishers/code/${code}/posts`,
    method: 'get',
    params: {
      pageIndex,
      pageSize,
      lastId
    }
  })
