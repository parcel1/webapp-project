import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getSuggestion = (keyword) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/search/suggestion`,
    method: 'get',
    params: {
      keyword
    }
  })

export const searchTopic = (type, keyword, pageSize, pageIndex) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/search/${type}`,
    method: 'get',
    params: {
      keyword,
      pageSize,
      pageIndex
    }
  })
