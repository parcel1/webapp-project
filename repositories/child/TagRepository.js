import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getHashtagsTrending = () =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/hashtags/top/view`,
    method: 'get'
  })

export const getTagDetail = (id) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/hashtag/${id}`,
    method: 'get'
  })

export const getPostsByTagFirst = (pageSize, pageIndex, code) => {
  return service({
    url: `/v3.0/profile/${getCookie('profileId')}/hashtag/${code}/posts`,
    method: 'get',
    params: {
      pageIndex,
      pageSize
    }
  })
}

export const getPostsByTag = (pageSize, pageIndex, code, lastId) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/hashtag/${code}/posts`,
    method: 'get',
    params: {
      pageIndex,
      pageSize,
      lastId
    }
  })
