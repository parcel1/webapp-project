import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getTrendPosts = (id, pageSize, pageIndex) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/trend/${id}/posts`,
    method: 'get',
    params: {
      pageIndex,
      pageSize
    }
  })
