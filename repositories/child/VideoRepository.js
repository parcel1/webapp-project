import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getVideoDetail = (slug) =>
  service({
    url: `/v4.0/profile/${getCookie('profileId')}/videos/short-id/${slug}`,
    method: 'get'
  })

// old
export const getVideoById = (id) =>
  service({
    url: `/v4.0/profile/${getCookie('profileId')}/videos/${id}/web`,
    method: 'get'
  })

export const getVideosRecommend = (id) =>
  service({
    url: `/v2.0/profile/${getCookie('profileId')}/videos/${id}/recommend`,
    method: 'get'
  })
