import { getCookie } from '@/utils/cookies'
import { service } from '../BaseRepository'

export const getWeatherDefault = () =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/weather/information`,
    method: 'get'
  })

export const getWeatherProvince = (provinceCode) =>
  service({
    url: `/v3.0/profile/${getCookie('profileId')}/weather/information`,
    method: 'get',
    params: {
      provinceCode
    }
  })
