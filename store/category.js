import { getAllCategories } from '@/repositories/child/CategoryRepository'
export const state = () => ({
  categories: []
})

export const getters = {
  categories: (s) => s.categories
}

export const mutations = {
  SET_CATEGORIES (state, categories) {
    state.categories = [...categories]
  }
}

export const actions = {
  async setCategories ({ commit, rootState }) {
    try {
      const categories = await getAllCategories(rootState.profileId)
      commit('SET_CATEGORIES', categories.data.Data)
    } catch (error) { }
  }
}
