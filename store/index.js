import { newUUID } from '../utils/uuid';
export const ProfileMutations = {
  PROFILEID: 'profileId',
  SET_PROFILEID: 'setProfileId'
}


export const state = () => ({
  profileId: null,
  host: ''
})

export const getters = {
  profileId: (s) => s.profileId,
  host: (s) => s.host
}

export const mutations = {
  setProfileId (state, profileId) {
    state.profileId = profileId
  },
  SET_HOST (state, host) {
    state.host = host
  }
}

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    commit('SET_HOST', req.headers.host)
    if (this.$cookiz.get(ProfileMutations.PROFILEID)) {
      commit(ProfileMutations.SET_PROFILEID, this.$cookiz.get(ProfileMutations.PROFILEID))
    } else {
      const newProfileId = newUUID()
      this.$cookiz.set(ProfileMutations.PROFILEID, newProfileId)
    }
  }
}
