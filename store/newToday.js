import { getNewsToday, getVideoNewsToday } from '@/repositories/child/PostRepository'
export const state = () => ({
  newsToday: [],
  videoNewsToday: []
})

export const getters = {
  newsToday: (s) => s.newsToday,
  videoNewsToday: (s) => s.videoNewsToday
}

export const mutations = {
  SET_NEWS_TODAY (state, newsToday) {
    state.newsToday = [...newsToday]
  },
  SET_VIDEO_NEWS_TODAY (state, videoNewsToday) {
    state.videoNewsToday = [...videoNewsToday]
  }
}

export const actions = {
  async setNewsToday ({ commit }) {
    try {
      const [newsToday, videoNewsToday] = await Promise.all([getNewsToday(), getVideoNewsToday()])
      commit('SET_NEWS_TODAY', newsToday.data.Data)
      commit('SET_VIDEO_NEWS_TODAY', videoNewsToday.data.Data)
    } catch (error) { }
  }
}
