import { getHashtagsTrending } from '@/repositories/child/TagRepository'
export const state = () => ({
  tags: []
})

export const getters = {
  tags: (s) => s.tags
}

export const mutations = {
  SET_TAGS (state, tags) {
    state.tags = [...tags]
  }
}

export const actions = {
  async setTags ({ commit }) {
    try {
      const tags = await getHashtagsTrending()
      commit('SET_TAGS', tags.data.Data)
    } catch (error) { }
  }
}
