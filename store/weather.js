import { getWeatherProvince } from '@/repositories/child/WeatherRepository'
export const state = () => ({
  weather: null,
})

export const getters = {
  weather: (s) => s.weather,
}

export const mutations = {
  SET_WEATHER (state, weather) {
    state.weather = weather
  }
}

export const actions = {
  async setWeather ({ commit }) {
    try {
      if (this.$cookiz.get('provinceCode')) {
        const weather = await getWeatherProvince(this.$cookiz.get('provinceCode'))
        commit('SET_WEATHER', weather.data.Data)
      } else {
        this.$cookiz.set('provinceCode', 'HaNoi')
        const weather = await getWeatherProvince('HaNoi')
        commit('SET_WEATHER', weather.data.Data)
      }
    } catch (error) { }
  }
}
