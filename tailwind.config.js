/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  extend: {
    spacing: {
      '1/2': '50%',
      '1/3': '33.333333%',
      '2/3': '66.666667%',
      '1/4': '25%',
      '2/4': '50%',
      '3/4': '75%',
      '1/5': '20%',
      '2/5': '40%',
      '3/5': '60%',
      '4/5': '80%',
      '1/6': '16.666667%',
      '2/6': '33.333333%',
      '3/6': '50%',
      '4/6': '66.666667%',
      '5/6': '83.333333%',
      '1/12': '8.333333%',
      '2/12': '16.666667%',
      '3/12': '25%',
      '4/12': '33.333333%',
      '5/12': '41.666667%',
      '6/12': '50%',
      '7/12': '58.333333%',
      '8/12': '66.666667%',
      '9/12': '75%',
      '10/12': '83.333333%',
      '11/12': '91.666667%'
    }
  },
  theme: {
    fontSize: {
      xs: '.75rem',
      sm: '.875rem',
      // tiny: '.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '2.5xl': '1.75rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem'
    },
    truncate: {
      lines: {
        2: '2',
        3: '3'
      }
    },
    colors: {
      transparent: 'transparent',
      black: '#000',
      white: '#fff',
      gray: {
        100: '#f5f5f4',
        200: '#635b5a',
        300: '#e9e8e7',
        400: '#9c9796',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#392f2d',
        900: '#160f0e'
      },
      red: {
        100: '#f6f6f6',
        200: 'rgba(232, 84, 52, 0.1)',
        300: '#feb2b2',
        400: '#fc8181',
        500: '#f56565',
        600: '#e5e5e5',
        700: '#c53030',
        800: '#9b2c2c',
        900: '#742a2a'
      },
      orange: {
        100: '#fffaf0',
        200: '#fbf4e0',
        300: '#fbd38d',
        400: '#f6ad55',
        500: '#e85434',
        600: '#dd6b20',
        700: '#c05621',
        800: '#9c4221',
        900: '#7b341e'
      },
      yellow: {
        100: '#fffff0',
        200: '#fefcbf',
        300: '#faf089',
        400: '#f6e05e',
        500: '#ecc94b',
        600: '#d69e2e',
        700: '#b7791f',
        800: '#975a16',
        900: '#744210'
      },
      green: {
        100: '#f0fff4',
        200: '#c6f6d5',
        300: '#9ae6b4',
        400: '#68d391',
        500: '#48bb78',
        600: '#38a169',
        700: '#2f855a',
        800: '#276749',
        900: '#22543d'
      },
      teal: {
        100: '#e6fffa',
        200: '#b2f5ea',
        300: '#81e6d9',
        400: '#4fd1c5',
        500: '#38b2ac',
        600: '#319795',
        700: '#2c7a7b',
        800: '#285e61',
        900: '#234e52'
      },
      blue: {
        100: '#ebf8ff',
        200: '#bee3f8',
        300: '#90cdf4',
        400: '#63b3ed',
        500: '#4299e1',
        600: '#3182ce',
        700: '#2b6cb0',
        800: '#2c5282',
        900: '#2a4365'
      },
      indigo: {
        100: '#ebf4ff',
        200: '#c3dafe',
        300: '#a3bffa',
        400: '#7f9cf5',
        500: '#667eea',
        600: '#5a67d8',
        700: '#4c51bf',
        800: '#434190',
        900: '#3c366b'
      },
      purple: {
        100: '#faf5ff',
        200: '#e9d8fd',
        300: '#d6bcfa',
        400: '#b794f4',
        500: '#9f7aea',
        600: '#805ad5',
        700: '#6b46c1',
        800: '#553c9a',
        900: '#44337a'
      },
      pink: {
        100: '#fff5f7',
        200: '#fed7e2',
        300: '#fbb6ce',
        400: '#f687b3',
        500: '#ed64a6',
        600: '#d53f8c',
        700: '#b83280',
        800: '#97266d',
        900: '#702459'
      }
    },
    extend: {
      width: {
        28: '7rem',
        58: '15rem'
      },
      padding: {
        0: '0rem',
        19: '4.75rem',
        22: '5.5rem'
      },
      lineHeight: {
        5.5: '1.375rem',
        7.5: '1.875rem',
        11: '2.75rem',
        12: '3rem'
      },
      zIndex: {
        100: '100',
        200: '200',
        999: '999'
      },
      keyframes: {
        fade: {
          '0%': { transform: 'scale(1)' },
          '100%': { transform: 'scale(1.2)' }
        }
      },
      animation: {
        fade: 'fade 4s'
      },
      inset: {
        16: '4rem',
        22: '5.5rem',
        '-4': '-1rem',
        '-5': '-1.25rem',
        6: '-1.5rem',
        '1/2': '50%'
      },
      colors: {
        orangeHit: {
          100: 'rgba(232, 84, 52, 0.05)',
          200: 'rgba(232, 84, 52, 0.1)',
          300: '#F6F4F4',
          400: 'rgba(232, 84, 52, 0.3)',
          500: 'rgba(232, 84, 52, 0.8)',
          600: '#E85434'
        },
        blackHit: {
          100: 'rgba(22, 15, 14, 0.1)',
          200: 'rgba(22, 15, 14, 0.4)',
          300: 'rgba(22, 15, 14, 0.7)',
          400: 'rgba(22, 15, 14, 0.9)',
          500: '#160F0E'
        },
        whiteHit: {
          100: 'rgba(255, 255, 255, 0.1)',
          200: 'rgba(255, 255, 255, 0.4)',
          300: 'rgba(255, 255, 255, 0.7)',
          400: 'rgba(255, 255, 255, 0.9)',
          500: '#FFFFFF'
        },
        neutralLightHit: {
          100: '#E9E8E7',
          200: '#D2D0D0',
          300: '#9C9796',
          400: '#635B5A',
          500: '#392F2D',
          600: '#160F0E'
        },
        neutralDarkHit: {
          100: 'rgba(255, 255, 255, 0.1)',
          200: 'rgba(255, 255, 255, 0.2)',
          300: 'rgba(255, 255, 255, 0.4)',
          400: 'rgba(255, 255, 255, 0.6)',
          500: 'rgba(255, 255, 255, 0.8)',
          600: '#FFFFFF'
        }
      }
    }
  },
  variants: {
    margin: ['responsive', 'first', 'last', 'hover', 'focus'],
    overflow: ['responsive', 'hover'],
    padding: ['responsive'],
    borderRadius: ['responsive', 'hover', 'focus']
  },
  plugins: [require('tailwindcss-truncate-multiline')(['responsive', 'hover'])],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: ['components/**/*.vue', 'layouts/**/*.vue', 'pages/**/*.vue', 'plugins/**/*.js', 'nuxt.config.js']
  }
}
