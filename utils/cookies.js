import cookie from 'js-cookie'

export const setCookie = (key, value) => {
  cookie.set(key, value, {
    expires: 365,
    path: '/'
  })
}

export const removeCookie = (key) => {
  if (process.browser) {
    cookie.remove(key, {
      expires: 1
    })
  }
}

export const getCookie = (key, req) => {
  return getCookieFromBrowser(key)
}

export const getProvince = (key, req) => {
  return cookie.get('provinceCode')
}

export const getCookieFromBrowser = (key) => {
  return cookie.get(key)
}
