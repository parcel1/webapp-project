import _formatDistanceToNow from 'date-fns/formatDistanceToNow'
// Require Esperanto locale
import { vi } from 'date-fns/locale'

export const toTimeAgo = (timestamp) => {
  return _formatDistanceToNow(Number(timestamp * 1000), { locale: vi })
}
