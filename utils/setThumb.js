export const setThumb = (url, size) => {
  const arr = url.split('/')
  arr.splice(arr.length - 1, 0, size)
  return arr.join('/').toString()
}
