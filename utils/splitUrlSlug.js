export const splitUrl = (url) => {
  const pieces = url.split(/[\s-]+/)
  const slug = pieces[pieces.length - 1]
  return slug
}
