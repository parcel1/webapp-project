import short from 'short-uuid'
const translator = short()

export const toUUID = (id) => {
  return translator.toUUID(id)
}

export const fromUUID = (id) => {
  if (id !== undefined) {
    return translator.fromUUID(id)
  }
}

export const newUUID = () => {
  return translator.uuid()
}
